import os
from datetime import date


today = date.today()
d1 = today.strftime("%d-%m-%Y")
dir_path = f"results/{d1}"


def criar_diretorio():
    try:
        os.mkdir("results")
    except FileExistsError:
        pass
    try:
        os.mkdir(dir_path)
    except FileExistsError:
        pass


def salva_content(filename, content):
    with open(f'{dir_path}/{filename}', 'wb') as f:
        f.write(content)
