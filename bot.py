import requests
import argparse
import parser
import helper
import datetime
import sys
import re


def _first_request():
    url = "https://www.gov.br/anvisa/pt-br/assuntos/medicamentos/cmed/precos/anos-anteriores/anos-anteriores"

    headers = {
        "Host": "www.gov.br",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive"
    }

    first_request = requests.get(url, headers=headers)
    helper.salva_content("first_page.html", first_request.content)

    return first_request.content


def _download_file_request(download_links):
    headers = {
        "Host": "www.gov.br",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Referer": "https://www.gov.br/anvisa/pt-br/assuntos/medicamentos/cmed/precos/anos-anteriores/anos-anteriores",
        "Connection": "keep-alive"
    }

    for url in download_links:
        filename = url.rsplit("/", 1)[1]
        download_request = requests.get(url, headers=headers)
        helper.salva_content(f"{filename}.xls", download_request.content)

    return True


def _handle_inputs():
    inputs = argparse.ArgumentParser(description="ess-test")
    inputs.add_argument("--start_year", dest="start_year", type=str)
    inputs.add_argument("--end_year", dest="end_year", type=str)

    args = inputs.parse_args()
    if args.start_year and args.end_year:
        return f"abril/{args.start_year[2:]}", f"março/{args.end_year[2:]}"
    sys.exit("invalid inputs")


def main():
    helper.criar_diretorio()
    start, end = _handle_inputs()

    fr = _first_request()
    download_links = parser.parse_content(fr, start, end)
    if (download_links):
        get_files = _download_file_request(download_links)
        sys.exit('files downloaded')

    sys.exit('files not found')


if __name__ == "__main__":
    main()
