import re
import json
from bs4 import BeautifulSoup


def parse_content(content, start_period, end_period):
    download_links = []
    soup = BeautifulSoup(content, "html.parser")

    div = soup.find("div", {"class": "journal-content-article"})
    p = div.find_all("p")
    for element in p:
        e = element.strong
        if (e):
            period = e.text
            if (start_period in period or end_period in period):
                for a in element.find_all("a", href=True, text=True):
                    if ("XLS" in a.text):
                        download_links.append(a["href"])

    return download_links



