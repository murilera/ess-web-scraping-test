# ess-web-scraping-test


## Description
Pre-Employment test for ESS. <br />
Scraped website: [https://www.gov.br/anvisa/pt-br/assuntos/medicamentos/cmed/precos/anos-anteriores/anos-anteriores](https://www.gov.br/anvisa/pt-br/assuntos/medicamentos/cmed/precos/anos-anteriores/anos-anteriores)

## Installation
Install python3 <br />
Install requirements.txt <br />

## Usage
Bot needs both inputs: start_year and end_year. Example: 2021, 2020, 2019, 2018...

Run python3 bot.py --start_year 2021 --end_year 2021


## To do
Maybe adjust inputs <br />
Tests <br />
Multi-threading download requests <br />


## Authors and acknowledgment
Murilo Rodegheri Mendes dos Santos